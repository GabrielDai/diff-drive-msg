# Differential Drive Robot messages

This ROS2 packet contains the used messages by
[twheelsbot](https://gitlab.com/GabrielDai/twheelsbot).

## Build with colcon

``` shell
colcon build --packages-select diff_drive_msg
```
